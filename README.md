This plugin was created using the Atlassian SDK's `atlas-create-bitbucket-plugin` command to demonstrate problems building it with plain maven.

To reproduce the problem, simply move your local maven settings & repository to a new directory to make sure they don't 
provide the missing dependency. e.g.

`mv ~/.m2 ~/.m2-temporary-backup`

On my mac, I used IntelliJ IDEA's bundled maven 3 to run the plugin:
`/Applications/IntelliJ\ IDEA.app/Contents/plugins/maven/lib/maven3/bin/mvn bitbucket:debug`

The error seems to occur despite having Atlassian's plugin repositories in the pom.xml file. 
It only occurs during the bitbucket:debug goal; I can package or compile the plugin just fine, which makes me think the 
problem is not a maven problem, but something specifically wrong with the bitbucket parent pom. This doesn't happen in 
comparable plugins for Jira or Confluence.

```
[WARNING] Failed to build parent project for com.atlassian.pom:closedsource-pom:pom:5.0.8
[WARNING] Failed to build parent project for com.atlassian.bitbucket.server:bitbucket-parent:pom:5.8.0
[INFO] ------------------------------------------------------------------------
[INFO] BUILD FAILURE
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 04:51 min
[INFO] Finished at: 2018-10-11T12:51:48-05:00
[INFO] Final Memory: 63M/240M
[INFO] ------------------------------------------------------------------------
[ERROR] Failed to execute goal com.atlassian.maven.plugins:bitbucket-maven-plugin:6.3.15:debug (default-cli) on project example: Could not build the MavenProject for com.atlassian.bitbucket.server:bitbucket-parent:pom:5.8.0: Some problems were encountered while processing the POMs:
[ERROR] [ERROR] Unresolveable build extension: Plugin com.atlassian.maven.plugins:artifactory-staging-maven-plugin:1.0.3 or one of its dependencies could not be resolved: Could not find artifact com.atlassian.maven.plugins:artifactory-staging-maven-plugin:jar:1.0.3 in central (https://repo.maven.apache.org/maven2) @: 1 problem was encountered while building the effective model for com.atlassian.bitbucket.server:bitbucket-parent:5.8.0
[ERROR] [ERROR] Unresolveable build extension: Plugin com.atlassian.maven.plugins:artifactory-staging-maven-plugin:1.0.3 or one of its dependencies could not be resolved: Could not find artifact com.atlassian.maven.plugins:artifactory-staging-maven-plugin:jar:1.0.3 in central (https://repo.maven.apache.org/maven2) @
[ERROR] -> [Help 1]
[ERROR] 
[ERROR] To see the full stack trace of the errors, re-run Maven with the -e switch.
[ERROR] Re-run Maven using the -X switch to enable full debug logging.
[ERROR] 
[ERROR] For more information about the errors and possible solutions, please read the following articles:
[ERROR] [Help 1] http://cwiki.apache.org/confluence/display/MAVEN/MojoExecutionException
```

Complete debug logs from a run with a totally clean local maven settings (I deleted ~/.m2) are in the complete_debug.log 
file.

I'm no maven expert, but if I had to guess, I would say this line from the `bitbucket:debug` goal's config
may be part of the problem:
`<repositories>${project.remoteArtifactRepositories}</repositories>`

It seems likely that this could be overriding the repositories specified in my project's pom, thus preventing them from
being used during resolution. This further accounts for why, if you put Atlassian's maven repositories in your own local
maven settings.xml, the problem goes away. The `bitbucket:debug` goal is unique in overriding the `<repositories>` tag.

We're trying to give our users a sample Bitbucket server plugin based on ours that they can build form scratch with pure 
maven, and this prevents that.
